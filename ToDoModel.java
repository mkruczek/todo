package com.example.kruczek.mytodo;

/**
 * Created by kruczek on 06/07/17.
 */

public class ToDoModel {

    private int id;
    private String title;
    private String description;
    private String date;

    public ToDoModel(String title) {
        this.title = title;
    }

    public ToDoModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String shortPrint(){
        return getDescription().substring(0,50);
    }
}
