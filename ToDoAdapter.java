package com.example.kruczek.mytodo;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by kruczek on 06/07/17.
 */

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.MyViewHolder> {

    private List<ToDoModel> toDoModelList;

    public ToDoAdapter(List<ToDoModel> toDoModelList) {
        this.toDoModelList = toDoModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.one_todo_row, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        ToDoModel toDoModel = toDoModelList.get(position);

        holder.title.setText(toDoModel.getTitle());
        holder.description.setText(toDoModel.getDescription());
        holder.date.setText(toDoModel.getDate());

        // add icon on ImageView

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout layout = new LinearLayout(v.getContext());
                layout.setOrientation(LinearLayout.VERTICAL);

                Switch editOnOF = new Switch(v.getContext());
                editOnOF.setText("Włącz edycję");
                layout.addView(editOnOF);

                final EditText dialogEditId = new EditText(v.getContext());
                dialogEditId.setHint("Test");
                layout.addView(dialogEditId);

                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setView(layout);
                alertDialog.setTitle("Podgląd Zadania").
                        setPositiveButton("Akceptuj", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        }).setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                // create AlertDialog to watch and edit Task
            }
        });

    }

    @Override
    public int getItemCount() {
        return toDoModelList.size();
    }




    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon;
        public TextView title;
        public TextView description;
        public TextView date;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.imageViewIcon);
            title = (TextView) itemView.findViewById(R.id.textViewTitle);
            description = (TextView) itemView.findViewById(R.id.textViewDescription);
            date = (TextView) itemView.findViewById(R.id.textViewData);
        }


    }
}
