package com.example.kruczek.mytodo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kruczek on 06/07/17.
 */

public class HelperDB extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ToDoDataBase.db";

    public static final String SQL_CREATE_ENTRIES =
                    "CREATE TABLE " + ToDoContract.ToDoTable.TABLE_NAME
                    + " (" + ToDoContract.ToDoTable._ID + " INTEGER PRIMARY KEY,"
                    + ToDoContract.ToDoTable.TABLE_TITLE + " TEXT,"
                    + ToDoContract.ToDoTable.TABLE_DESCRIPTION + " TEXT,"
                    + ToDoContract.ToDoTable.TABLE_DATE + " TEXT)";

    public HelperDB (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
