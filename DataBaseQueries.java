package com.example.kruczek.mytodo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kruczek on 06/07/17.
 */

public class DataBaseQueries {

    private SQLiteDatabase database;
    private HelperDB helperDB;

    public DataBaseQueries(Context context) {
        helperDB = new HelperDB(context);
    }

    public void openSave() throws SQLException {
        database = helperDB.getWritableDatabase();
    }

    public void openRead() throws SQLException {
        database = helperDB.getReadableDatabase();
    }

    public void closeDB() {
        database.close();
    }

    public void addTask(String title, String description, String date) {
        ContentValues values = new ContentValues();

        values.put(ToDoContract.ToDoTable.TABLE_TITLE, title);
        values.put(ToDoContract.ToDoTable.TABLE_DESCRIPTION, description);
        values.put(ToDoContract.ToDoTable.TABLE_DATE, date);

        long rowId = database.insert(ToDoContract.ToDoTable.TABLE_NAME, null, values);
    }

    public List<ToDoModel> showAllTasks() {

        ArrayList<ToDoModel> allTasksList = new ArrayList<>();

        Cursor cursor = database.query(ToDoContract.ToDoTable.TABLE_NAME, null, null, null, null, null, null);

        ToDoModel singleTask;
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                singleTask = new ToDoModel();
                singleTask.setId(cursor.getInt(0));
                singleTask.setTitle(cursor.getString(1));
                singleTask.setDescription(cursor.getString(2));
                singleTask.setDate(cursor.getString(3));

                allTasksList.add(singleTask);
            }
        }
        return allTasksList;
    }


}
