package com.example.kruczek.mytodo;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class ToDoActivity extends AppCompatActivity {

    private String title;
    private String description;
    private String date;
    private List<ToDoModel> allTasksList;

    private DataBaseQueries connectionDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        connectionDB = new DataBaseQueries(ToDoActivity.this);
        connectionDB.openSave();

        allTasksList = connectionDB.showAllTasks();

        final RecyclerView myRecycler = (RecyclerView) findViewById(R.id.myRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        myRecycler.setLayoutManager(llm);
        final ToDoAdapter adapter = new ToDoAdapter(allTasksList);
        myRecycler.setAdapter(adapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout layout = new LinearLayout(ToDoActivity.this);
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText dialogTitle = new EditText(ToDoActivity.this);
                dialogTitle.setHint("Tytuł");
                layout.addView(dialogTitle);

                final EditText dialogDescription = new EditText(ToDoActivity.this);
                dialogDescription.setHint("Opis");
                layout.addView(dialogDescription);

                final EditText dialogDate = new EditText(ToDoActivity.this);
                dialogDate.setHint("Data: yyyy-mm-dd");
                layout.addView(dialogDate);


                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ToDoActivity.this);
                alertDialog.setView(layout);
                alertDialog.setTitle("Dodaj zadanie.")
                        .setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                title = dialogTitle.getText().toString();
                                description = dialogDescription.getText().toString();
                                date = dialogDate.getText().toString();

                                connectionDB.addTask(title, description, date);

                                myRecycler.setAdapter(new ToDoAdapter(connectionDB.showAllTasks()));
                            }
                        })
                        .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).
                        setCancelable(true).show();
            }
        });

    }
}
