package com.example.kruczek.mytodo;

import android.provider.BaseColumns;

/**
 * Created by kruczek on 06/07/17.
 */

public class ToDoContract {

    public ToDoContract() {
    }

    public final class ToDoTable implements BaseColumns {
        public static final String TABLE_NAME = "ToDoList";
        public static final String TABLE_TITLE = "Tytuł";
        public static final String TABLE_DESCRIPTION = "Opis";
        public static final String TABLE_DATE = "Termin";
    }
}
